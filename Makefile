##
## Makefile for game in /home/camill_n/rendu/game_test
##
## Made by Nicolas Camilli
## Login   <camill_n@epitech.net>
##
## Started on  Thu Dec  5 14:44:13 2013 Nicolas Camilli
## Last update Tue Dec 10 23:44:53 2013 Nicolas Camilli
##

SRCS  	= main.c \
	  src/init.c \
	  src/hook.c \
	  src/anim.c \
	  src/client.c \
	  src/player.c \

RM	= rm -f

NAME	= game

CC	= cc

OBJS	= $(SRCS:.c=.o)

CFLAGS = -I./includes

all: 	$(NAME)

.c.o:
	$(CC) $(CFLAGS) -c $< -o $(<:.c=.o)

$(NAME): $(OBJS)
	 $(CC) $(OBJS) -o $(NAME) -lSDL -lSDL_image -lSDL_net

clean:
	$(RM) $(OBJS)

fclean:	clean
	$(RM) $(NAME)

re: fclean all
