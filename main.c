/*
** main.c for my_ls in /home/camill_n/rendu/PSU_2018_my_printf
**
** Made by Nicolas Camilli
** Login   <camill_n@epitech.net>
**
** Started on  Wed Nov 20 01:38:01 2013 Nicolas Camilli
** Last update Wed Dec 11 14:06:50 2013 Nicolas Camilli
*/

#include "init.h"
#include "hook.h"
#include "client.h"

int		main(int ac, char **av)
{
  t_global	global;

  connect_player(&global.sock);
  content(&global);
  return (0);
  init_SDL(&global);
  init_players(&global, 3);
  list_player(&global);
  pause(&global);
  /* deconnect_player(&global.sock); */
  SDL_Quit();
  return (0);
}
