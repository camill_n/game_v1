/*
** player.c for server in /home/camill_n/rendu/game_v1/server
** 
** Made by Nicolas Camilli
** Login   <camill_n@epitech.net>
** 
** Started on  Wed Dec 11 18:47:06 2013 Nicolas Camilli
** Last update Wed Dec 11 20:28:43 2013 Nicolas Camilli
*/

#include <SDL/SDL_net.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "server.h"

void		add_player(t_player_s **players, IPaddress *ip_players, int id)
{
  t_player_s	*new_player;

  static int	i = 0;

  if (id != -1)
    {
      while (id < i)
	{
	  players[id] = players[id + 1];
	  if (id != i - 1)
	    players[id]->id = id;
	  id++;
	}
      --i;
    }
  else
    {
      new_player = malloc(sizeof(t_player_s *));
      new_player->id = i;
      new_player->name = "Nico";
      new_player->x = W / 2;
      new_player->y = H / 2;
      new_player->ip_players = ip_players;
      players[i] = new_player;
      players[++i] = NULL;
    }
}
