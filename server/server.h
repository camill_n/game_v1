/*
** server.h for server in /home/camill_n/rendu/game_v1/server
**
** Made by Nicolas Camilli
** Login   <camill_n@epitech.net>
**
** Started on  Wed Dec 11 18:19:18 2013 Nicolas Camilli
** Last update Wed Dec 11 19:42:05 2013 Nicolas Camilli
*/

#ifndef SERVER_H_
# define SERVER_H_

#define W 1920
#define H 1080

typedef struct	s_player_s
{
  int		id;
  char		*name;
  int		x;
  int		y;
  IPaddress	*ip_players;
}		t_player_s;


void            add_player(t_player_s **players, IPaddress *ip_players, int id);
int             nb_player(t_player_s **players);

#endif
