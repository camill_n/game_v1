/*
** hook.c for game in /home/camill_n/rendu/game_test
**
** Made by Nicolas Camilli
** Login   <camill_n@epitech.net>
**
** Started on  Thu Dec  5 15:09:53 2013 Nicolas Camilli
** Last update Wed Dec 11 01:30:27 2013 BOUSCAREL Antonin
*/

#include "init.h"

void		set_back(SDL_Surface *ecran)
{
  SDL_FillRect(ecran, NULL, SDL_MapRGB(ecran->format, 66, 66, 33));
}

void		check_wall(t_global *global, int i)
{
  global->players[i]->x < 10 ? touch_wall(global, i) : 0;
  global->players[i]->x > (W - 60) ? touch_wall(global, i) : 0;
  global->players[i]->y < 10 ? touch_wall(global, i) : 0;
  global->players[i]->y > (H - 110) ? touch_wall(global, i) : 0;
}

void		update_player(t_global *global, int i, int movement, char mode, int view)
{
  if (mode == 'X')
    {
      if (movement < 0 && global->players[i]->x > 5)
	global->players[i]->x += movement;
      if (movement > 0 && global->players[i]->x < W - 50)
	global->players[i]->x += movement;
    }
  else if (mode == 'Y')
    {
      if (movement < 0 && global->players[i]->y > 5)
	global->players[i]->y += movement;
      if (movement > 0 && global->players[i]->y < H - 100)
	global->players[i]->y += movement;
    }
  check_wall(global, i);
  set_back(global->ecran);
  set_image(global, view, i);
  list_player(global);
}

void		use_weapon(t_global *global, int i)
{
  SDL_Rect	posfire;
  SDL_Surface	*fire;

  fire = IMG_Load("images/tir2.png");
  posfire.x = global->players[i]->x;
  posfire.y = global->players[i]->y;

  while (posfire.x < W)
    {
      set_back(global->ecran);
      list_player(global);
      SDL_BlitSurface(fire, NULL, global->ecran, &posfire);
      SDL_Flip(global->ecran);
      posfire.x += 5;
    }
}

void		pause(t_global *global)
{
  int		check;
  SDL_Event	event;
  int		i;
  SDL_Rect      positionFond;

  i = 1;
  check = 1;
  SDL_EnableKeyRepeat(15, 15);
  while (check)
    {
      SDL_WaitEvent(&event);
      event.type == SDL_QUIT ? (check = 0) : 0;
      event.key.keysym.sym == SDLK_ESCAPE ? (check = 0) : 0;
      if (event.type == SDL_KEYDOWN)
	{
	  if (event.key.keysym.sym == SDLK_a)
	    use_weapon(global, i);
	  if (event.key.keysym.sym == SDLK_UP)
	    update_player(global, i, -10, 'Y', UP);
	  if (event.key.keysym.sym == SDLK_DOWN)
	    update_player(global, i, 10, 'Y', DOWN);
	  if (event.key.keysym.sym == SDLK_RIGHT)
	    update_player(global, i, 10, 'X', RIGHT);
	  if (event.key.keysym.sym == SDLK_LEFT)
	    update_player(global, i, -10, 'X', LEFT);
	}
    }
}

