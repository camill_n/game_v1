/*
** anim.c for game in /home/camill_n/rendu/game_v1
** 
** Made by Nicolas Camilli
** Login   <camill_n@epitech.net>
** 
** Started on  Tue Dec 10 23:47:03 2013 Nicolas Camilli
** Last update Wed Dec 11 00:06:59 2013 Nicolas Camilli
*/

#include "init.h"

void		touch_wall(t_global *global, int i)
{
  static int	cpt = 0;
  int		x;
  int		y;

  x = global->players[i]->x;
  y = global->players[i]->y;
  while (x != W / 2 || y != H / 2)
    {
      if (x != W / 2)
	x > W / 2 ? (x = x - 5) : (x = x + 5);
      if (y != H / 2)
	y > H / 2 ? (y = y - 5) : (y = y + 5);
      set_back(global->ecran);
      global->players[i]->x = x;
      global->players[i]->y = y;
      cpt == 0 ? set_image(global, UP, i) : 0;
      cpt == 1 ? set_image(global, RIGHT, i) : 0;
      cpt == 2 ? set_image(global, LEFT, i) : 0;
      if (cpt == 3)
	{
	  set_image(global, DOWN, i);
	  cpt = -1;
	}
      list_player(global);
      ++cpt;
    }
  set_back(global->ecran);
  list_player(global);
}
