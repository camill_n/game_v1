/*
** init.c for game in /home/camill_n/rendu/game_test
**
** Made by Nicolas Camilli
** Login   <camill_n@epitech.net>
**
** Started on  Thu Dec  5 14:58:03 2013 Nicolas Camilli
** Last update Tue Dec 10 17:13:04 2013 BOUSCAREL Antonin
*/

#include "init.h"

int	init_SDL(t_global *global)
{
  if (SDL_Init(SDL_INIT_VIDEO) == -1)
    {
      printf("Problème lors de l'initialisation de la SDL\n");
      exit(0);
    }
  SDL_WM_SetCaption(T, NULL);
  global->ecran =
    SDL_SetVideoMode(W, H, 32, SDL_HWSURFACE | SDL_RESIZABLE | SDL_DOUBLEBUF);
  set_back(global->ecran);
  return (1);
}

int		init_players(t_global *global, int nb_players)
{
  int		i;
  static int	id = 1;

  global->players = malloc((nb_players + 1) * sizeof(t_player *));
  global->players == NULL ? exit(0) : 0;
  i = 0;
  while (i < nb_players)
    {
      global->players[i] = malloc(sizeof(t_player));
      global->players[i] == NULL ? (exit(0)) : 0;
      global->players[i]->name = "Nico";
      global->players[i]->x = 500 + (id * 50);
      global->players[i]->y = 500;;
      global->players[i]->id = id++;
      global->players[i]->img = IMG_Load("images/45.gif");
      i++;
    }
  global->players[i] = NULL;
  return (1);
}
