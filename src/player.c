/*
** player.c for game in /home/camill_n/rendu/game_test
**
** Made by Nicolas Camilli
** Login   <camill_n@epitech.net>
**
** Started on  Thu Dec  5 15:41:58 2013 Nicolas Camilli
** Last update Wed Dec 11 01:45:46 2013 BOUSCAREL Antonin
*/

#include "init.h"

void		set_image(t_global *global, int mode, int i)
{
  if (mode == DOWN)
    global->players[i]->img = IMG_Load("images/45.gif");
  if (mode == UP)
    global->players[i]->img = IMG_Load("images/48.gif");
  if (mode == RIGHT)
    global->players[i]->img = IMG_Load("images/46.gif");
  if (mode == LEFT)
    global->players[i]->img = IMG_Load("images/47.gif");
}

void		list_player(t_global *global)
{
  int		i;
  SDL_Rect	positionFond;

  i = 0;
  while (global->players[i] != NULL)
    {
      positionFond.x = global->players[i]->x;
      positionFond.y = global->players[i]->y;
      SDL_BlitSurface(global->players[i]->img,
		      NULL, global->ecran, &positionFond);
      printf("ID: %d\nPseudo: %s\nPos x: %d\nPos y: %d\n\n",
	     global->players[i]->id,
	     global->players[i]->name, global->players[i]->x, global->players[i]->y);
      i++;
    }
   SDL_Flip(global->ecran);
}
