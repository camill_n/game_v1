/*
** client.c for game in /home/camill_n/rendu/game_test
**
** Made by Nicolas Camilli
** Login   <camill_n@epitech.net>
**
** Started on  Tue Dec 10 00:18:39 2013 Nicolas Camilli
** Last update Tue Dec 10 23:14:16 2013 Nicolas Camilli
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <SDL/SDL_net.h>
#include "init.h"

int	connect_player(t_sock *sock)
{
  if (SDLNet_Init() < 0)
    {
      fprintf(stderr, "SDLNet_Init: %s\n", SDLNet_GetError());
      exit(EXIT_FAILURE);
    }
  if (SDLNet_ResolveHost(&sock->ip, "127.0.0.1", 3306) < 0)
    {
      fprintf(stderr, "SDLNet_ResolveHost: %s\n", SDLNet_GetError());
      exit(EXIT_FAILURE);
    }
  if (!(sock->sd = SDLNet_TCP_Open(&sock->ip)))
    {
      fprintf(stderr, "Serveur introuvable: %s\n", SDLNet_GetError());
      exit(EXIT_FAILURE);
    }
}

int	deconnect_player(t_sock *sock)
{
  SDLNet_TCP_Close(sock->sd);
  SDLNet_Quit();
}

int	content(t_global *global)
{
  int		quit;
  int		len;
  char		buffer[512];

  quit = 0;
  while (!quit)
    {
      printf("Write something:\n>");
      scanf("%s", buffer);
      len = strlen(buffer) + 1;
      if (SDLNet_TCP_Send(global->sock.sd, (void *)buffer, len) < len)
	{
	  fprintf(stderr, "SDLNet_TCP_Send: %s\n", SDLNet_GetError());
	  exit(EXIT_FAILURE);
	}
      if(strcmp(buffer, "exit") == 0)
	quit = 1;
      if(strcmp(buffer, "quit") == 0)
	quit = 1;
    }
  return (EXIT_SUCCESS);
}
