/*
** struct.h for game in /home/camill_n/rendu/game_test
**
** Made by Nicolas Camilli
** Login   <camill_n@epitech.net>
**
** Started on  Thu Dec  5 15:16:18 2013 Nicolas Camilli
** Last update Tue Dec 10 23:02:56 2013 Nicolas Camilli
*/

#include <SDL/SDL_net.h>

typedef struct	s_sock
{
  IPaddress	ip;
  TCPsocket	sd;
}		t_sock;

typedef struct	s_player
{
  int		id;
  char		*name;
  int		x;
  int		y;
  SDL_Surface	*img;
}		t_player;

typedef struct	s_global
{
  SDL_Surface	*ecran;
  t_player	**players;
  t_sock	sock;
}		t_global;
