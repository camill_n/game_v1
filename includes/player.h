/*
** player.h for game in /home/camill_n/rendu/game_test
** 
** Made by Nicolas Camilli
** Login   <camill_n@epitech.net>
** 
** Started on  Thu Dec  5 15:45:43 2013 Nicolas Camilli
** Last update Tue Dec 10 23:55:19 2013 Nicolas Camilli
*/

#ifndef PLAYER_H_
# define PLAYER_H

#define DOWN	1
#define UP	2
#define RIGHT	3
#define LEFT	4

void	set_image(t_global *global, int mode, int i);
void	list_player(t_global *global);
void	touch_wall(t_global *global, int i);

#endif
