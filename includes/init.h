/*
** init.h for game in /home/camill_n/rendu/game_test
**
** Made by Nicolas Camilli
** Login   <camill_n@epitech.net>
**
** Started on  Thu Dec  5 14:48:31 2013 Nicolas Camilli
** Last update Tue Dec 10 16:44:43 2013 BOUSCAREL Antonin
*/

#ifndef INIT_H_
# define INIT_H_

#define W 1920
#define H 1080
#define T "game"

#include <stdlib.h>
#include <stdio.h>
#include <SDL/SDL.h>
#include <SDL/SDL_image.h>
#include "struct.h"
#include "player.h"

int	init_SDL(t_global *global);
int	init_players(t_global *global, int nb_players);

#endif
