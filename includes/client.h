/*
** client.h for game in /home/camill_n/rendu/game_v1
**
** Made by Nicolas Camilli
** Login   <camill_n@epitech.net>
**
** Started on  Tue Dec 10 18:33:02 2013 Nicolas Camilli
** Last update Tue Dec 10 23:14:30 2013 Nicolas Camilli
*/

#ifndef CLIENT_H_
# define CLIENT_H_

int	content(t_global *global);
int	connect_player(t_sock *sock);
int	deconnect_player(t_sock *sock);

#endif
